(function() {
    var app = angular.module('booksWorld', ['ngAnimate']);

    app.animation('.slide-toggle', ['$animateCss', function($animateCss) {
        return {
            addClass: function(element, className, doneFn) {
                if (className == 'ng-hide') {
                    var animator = $animateCss(element, {                    
                        to: {height: '0px', opacity: 0}
                    });
                    if (animator) {
                        return animator.start().finally(function() {
                            element[0].style.height = '';
                            doneFn();
                        });
                    }
                }
                doneFn();
            },
            removeClass: function(element, className, doneFn) {
                if (className == 'ng-hide') {
                    var height = element[0].offsetHeight;
                    var animator = $animateCss(element, {
                        from: {height: '0px', opacity: 0},
                        to: {height: height + 'px', opacity: 1}
                    });
                    if (animator) {
                     return animator.start().finally(doneFn);
                    }
                }
                doneFn();
            }
        };
    }]);

    // app.directive('fileInput',['$parse',function($parse){
    //   return {
    //     restrict:'A',
    //     link:function(scope,elm,attrs){
    //       elm.bind('change',function(){
    //         $parse(attrs.fileInput)
    //         .assign(scope,elm[0].files)
    //         scope.$apply()
    //       })
    //     }
    //   }

    // }]);


    app.controller('Books_ctrl',['$scope','$http',function($scope,$http){

            $scope.buybook = function()
            {   
                     $('#buybook').modal('show');   
            }


                // $scope.files = [];
                // $scope.form = [];

                // $(window).on('load',function(){
                //      $('#warning').modal('show');
                // });
              
                // $scope.toggle = function()
                // {
                //     console.log();
                //     $('#warning').modal('show');
                // }
                // $scope.toggle1 = function(id)
                // {
                //     // console.log(id);
                //     $('#warning').modal('hide');
                // }
                // $scope.fileChanged = function(elm){
                //   $scope.file1 = elm.file1
                //   $scope.file2 = elm.file2
                //   $scope.file3 = elm.file3
                //   $scope.$apply();

                // }
                // $scope.save =function(){
                //   var fd = new FormData();
                //   fd.append("bookName", $scope.book.bookName);  
                //   fd.append("bookAuthor", $scope.book.bookAuthor);  
                //   fd.append("bookPrice", $scope.book.bookPrice); 
                //   angular.forEach($scope.file1,function(file){
                //     fd.append('file1',file)
                //   })

                //     angular.forEach($scope.file2,function(file){
                //     fd.append('file2',file)
                //   })

                //     angular.forEach($scope.file3,function(file){
                //     fd.append('file3',file)
                //   })
                //   $http.post('/books',fd,
                //   {
                //     transformRequest:angular.identity,
                //     headers:{'Content-Type':undefined}
                //   })
                //   .success(function(data){
                //     console.log(data)
                //   })
                // } 


                
            }]);

   


})();
