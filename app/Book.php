<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
	protected $fillable =  array('bookName','bookAuthor','bookPrice','user_id');
}
