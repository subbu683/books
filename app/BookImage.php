<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookImage extends Model
{
    //

    protected $fillable = array('file1','file2','file3','user_id');
}
