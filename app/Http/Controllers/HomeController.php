<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use App\User;
use Auth;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('welcome');
    }
    public function home()
    {
        return view('home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $email = $request->email;
        $password = $request->password;
        if(Auth::attempt(['email' => $email,'password' => $password]))
        {
            return redirect('/home');
        }
        else
        {
            return redirect()->back();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store_signup(Request $request)
    {
        //return dd($request->all());
        $validator =Validator::make($request->all(),[
            'name' => 'required |min:2',
            'rollnumber' => 'required|unique:users|alpha_num',
            'email' => 'required|unique:users|email',
            'password' => 'required|between:6,12',
            ]);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        else
        {
            $users = new User();
            $users->name = $request->name;
            $users->rollnumber = $request->rollnumber;
            $users->email = $request->email;
            $users->phonenumber = $request->phonenumber;
            $users->password = bcrypt($request->password);
            $users->save();

            // $request->session()->flash('success_signup','Successfully signedup.Please Login....');
            
            return redirect('/home');
        }
    }
    // public function store_signin(Request $request)
    // {
    //     $validator =Validator::make($request->all(),[
    //         'email' => 'required|email',
    //         'password' => 'required | min:6',

    //         ]);
    //     if($validator->fails())
    //     {
    //         return redirect()->back()->withErrors($validator)->withInput();
    //     }
    // }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
        Auth::logout();

        return redirect('/');
    }

    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
