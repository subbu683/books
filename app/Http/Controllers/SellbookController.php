<?php

namespace App\Http\Controllers;


use App\Book;
use App\BookImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\UploadedFile;
use Auth;
use Validator;

class SellbookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function selling(Request $request)
    {
      
          // return $request->all();  
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
     {
      // return dd($request->all());

     $id =Auth::id();
     //$b_id = DB::select('select id from books where user_id = ?', [$id]);
        // $b_id = BookImage::where('user_id', $id)->get();
        //     return dd($b_id);
     if($id == null)
     {
      
      $request->session()->flash("not_auth","Sorry..you must login first.");
      return redirect()->back();
     }else{

        $validator  = Validator::make($request->all(),[
          "bookName" => "required",
          "bookAuthor" => "required",
          "bookPrice" => "required",
          "file1" => "required",
          "file2" => "required",
          "file3" => "required",
        ]);
        if($validator->fails())
        {
          return redirect()->back()->withErrors($validator)->withInput();
        }
        else{

               $request->file('file1');

               //return $request->file1->store('public/upload');
                
              return Storage::putFile('public/upload',$request->file('file1'));

          // $book_details = new Book();
          // $book_details->bookName = $request->bookName;
          // $book_details->bookAuthor = $request->bookAuthor;
          // $book_details->bookPrice = $request->bookPrice;
          // $book_details->user_id = $id;
          // $book_details->save();

          // $book_images = new BookImage();
          // $book_images->file1 =$request->file1;
          // $book_images->file2 =$request->file2;
          // $book_images->file3 =$request->file3;
          // $book_images->size =223;
          // $book_images->user_id =$id;
          // $book_images->book_id =$id;
          // $book_images->save();

            $bookName = $request->bookName;
            $bookAuthor = $request->bookAuthor;
            $bookPrice = $request->bookPrice;
            $file1 = $request->file1;            
            $file2 = $request->file2;            
            $file3 = $request->file3;  

            $book = new Book();
            $book->bookName = $bookName;
            $book->bookAuthor = $bookAuthor;
            $book->bookPrice = $bookPrice;
            $book->user_id = $id;
            
            $book->save();

            $book_image = new BookImage();
           
           
            $file1 = $request->file('file1');
            return dd($file1);
           
            $book_image->file2 = $file2;
            $book_image->file3 = $file3;


            // if ($request->hasFile('file1'))
            // {
            //         $book_image->file1 = $request->file('file1')->store('public/upload');     
            // }

            // if ($request->hasFile('file2'))
            // {
            //         $book_image->file2 = $request->file('file2')->store('public/upload');     
            // }
            // if ($request->hasFile('file3'))
            // {
            //         $book_image->file3 = $request->file('file3')->store('public/upload');     
            // }
            $book_image->user_id = $id;
            $book_image->save();

          $request->session()->flash("book_success","Your book is successfully advertised.");

          return redirect()->back();

        }
     }
 }
        // $user = Auth::id();
        // // return $user;
        
        //       $bookName=$request->bookName;
        //       $bookAuthor=$request->bookAuthor;
        //       $bookPrice=$request->bookPrice;
        //       $file1 = $request->file1;
        //       $file2 = $request->file2;
        //       $file3 = $request->file3;
        //       $book = new Book();
        //       $book->bookName = $bookName;
        //       $book->bookAuthor = $bookAuthor;
        //       $book->bookPrice = $bookPrice;
        //       $book->user_id = $user;
        //       $book->save();

              

        //       if(!empty($file1)){
        //         $file1_name=$file1->getClientOriginalName();
        //         $file1->storeAs('public/upload',$file1_name);
        //         $file1_size=$file1->getClientSize();
        //         $book_images = new BookImage();
        //         $book_images->book = $file1_name;
        //         $book_images->size = $file1_size;
        //         $book_images->user_id = $user;
        //         $book_images->save();
        //       }
        //       else{
        //         return "Not Done";
        //       }
        //       if(!empty($file2)){
        //         $file2_name=$file2->getClientOriginalName();
        //         $file2->storeAs('public/upload',$file2_name);
        //         $file2_size=$file2->getClientSize();
        //         $book_images = new BookImage();
        //         $book_images->book = $file2_name;
        //         $book_images->size = $file2_size;
        //         $book_images->user_id = $user;
        //         $book_images->save();
        //       }
        //       else{
        //         return "Not Done";
        //       }
        //       if(!empty($file3)){
        //         $file3_name=$file3->getClientOriginalName();
        //         $file3->storeAs('public/upload',$file3_name);
        //         $file3_size=$file3->getClientSize();
        //         $book_images = new BookImage();
        //         $book_images->book = $file3_name;
        //         $book_images->size = $file3_size;
        //         $book_images->user_id = $user;
        //         $book_images->save();
        //       }
        //       else{
        //         return "Not Done";
        //       }
        //       return 'Done';


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
