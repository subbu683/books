<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use App\BookImage;

class WellcomeController extends Controller
{
    public function get_details(){

    	$books = Book::all();
    	return view('welcome',compact('books'));

    }

    public function gethom_details()
    {
    	$books_detials = Book::all();
    	return view('home',compact('books_detials'));
    }
    public function getimg_details()
    {
    	$book_images = BookImage::all();
        return dd($book_images);
    	return view('welcome',compact('book_images'));
    }
}
