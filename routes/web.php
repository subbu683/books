<?php

Route::get('/','HomeController@index');
Route::get('/','WellcomeController@get_details');
Route::get('/home','HomeController@home')->middleware('auth');
Route::post('/home','HomeController@store_signup');
Route::get('/home','WellcomeController@gethom_details')->middleware('auth');
Route::post('/login','HomeController@login');
Route::get('/logout','HomeController@logout');
Route::get('/bookdetails','BookdetailController@index');

Route::post('/','SellbookController@store');
Route::post('/books','SellbookController@store');
//Route::post('/books','SellbookController@storeimg');
// Route::post('/home','SignupController@store');
