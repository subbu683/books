<!doctype html>
<html lang="{{ app()->getLocale() }}" ng-app="booksWorld">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
       <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

        <!-- Styles -->
       <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
       <link rel="stylesheet" type="text/css" href="css/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <script type="text/javascript" src="js/jquery-ui.min.js"></script>
       
    </head>
     <body ng-controller="Books_ctrl">
       
    <div class="navbar-wrapper">
      <div class="container">

        <nav class="navbar navbar-static-top">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only" style="background:blue;">Toggle navigation</span>
                <span class="icon-bar" style="background:blue;"></span>
                <span class="icon-bar" style="background:blue;"></span>
                <span class="icon-bar" style="background:blue;"></span>
              </button>
              <a class="navbar-brand" href="#">Project name</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                <li class="active"><a href="#">Home</a></li>
                <li><a href="#about">About</a></li>
                <li><a href="#contact">Contact</a></li>
                 <li>
                  <!--  <button type="button" class="btn btn-primary navbar-btn" ng-click="toggle('1419A'); register=false; login=false" >SellBook</button> -->
                   <button type="submit" class="btn btn-primary navbar-btn" ng-click="sellbook=! sellbook; register=false; login=false">SellBook</button>
                 </li>
                 <li class="pull-right" style="margin-top: -15px;"><a href="/logout" ><button class="btn btn-danger navbar-btn">Logout</button></a></li>
              </ul>
              <div class="pull-right" style="margin-top: 10px;">
                <span class="icon nav-item">
                  <i class="fa fa-user-circle fa-2x"></i>
              </span>
              <span><b>MyAccount</b></span>
              </div>
            </div>
           
          </div>
            
        </nav>

      </div>
    </div>
      
    @if (Session::has('book_success'))                
      <div class="modal fade" id="warning"  tabindex="-1" role="dialog" aria-labelledby="sellbookLabel" aria-hidden="true">
                <div class="modal-dialog">
                
                  <div class="modal-content">
                    <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                    <div class="modal-body" style="text-align: center;">
                    <h1>
                    {{Session::get('book_success')}}
                    </h1>
                      </div>
                  </div>
                </div>
              </div>
        
      
        <script>
          $(window).load(function(){
          $('#warning').modal('show');
            
          });
        </script>
        @endif

        <div class="slide-toggle" ng-show="sellbook">
         <div class="col-md-4 col-sm-4 col-xs-2"></div>
        <div class="col-md-4 col-sm-4 col-xs-8">
          <form  method="POST" class="form-horizontal" action="/books" novalidate="">
          {{csrf_field()}}
             
             <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-book"></i></span>
                  <input id="bookName" type="text" class="form-control" placeholder="Enter Your BookTitle*...." name="bookName">
                  
            </div>

            <br>
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-pencil" aria-hidden="true"></i></span>
                  <input id="bookAuthor" type="text" class="form-control" placeholder="Enter Your BookAuthor*...." name="bookAuthor">
                </div><br>
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-inr" aria-hidden="true"></i></span>
                  <input id="bookPrice" type="text" class="form-control" placeholder="Enter Your BookPrice...." name="bookPrice">
                </div>
             
                <div class="fileUpload btn btn-warning">
                  <span><i class="fa fa-plus-square" aria-hidden="true"></i></span>
                   <span style="font-size:12px;"><b>Upload</b></span>
                  <input name="file1" type="file" class="form-control input-lg upload" accept="image/*"  multiple>
                </div>
                <div class="fileUpload btn btn-warning">
                  <span><i class="fa fa-plus-square" aria-hidden="true"></i></span>
                   <span style="font-size:12px;"><b>Upload</b></span>
                  <input name="file2" type="file" class="form-control input-lg upload" accept="image/*"  multiple>
                </div>
                <div class="fileUpload btn btn-warning">
                  <span><i class="fa fa-plus-square" aria-hidden="true"></i></span>
                   <span style="font-size:12px;"><b>Upload</b></span>
                  <input name="file3" type="file" class="form-control input-lg upload" accept="image/*"  multiple>
                </div>

             <br>
            <input type="submit" value="submit" class="btn btn-success" style="margin:0px -40px 0px 20px;">
            
            <br><br>
          </form>
          </div>
           <div class="col-md-4 col-sm-4 col-xs-2"></div>
        </div><!--sellbook-->

          <div class="container-fluid" id="showcase">
          
            <div class="showcase-item showcase-right" id="showcase-1" 
              style="background-image:url('images/1.jpg');">
              <div class="showcase-filter-area"></div>
              <div class="container">
                <div class="row">
                  <div class="column col-md-3 col-sm-2 hidden-xs"></div>
                  <div class="column col-md-9 col-sm-10 col-xs-12">                      
                    <div class="showcase-title"><b></b><p style="color:#e74c3c;font-size:38px;margin-left:250px;"></p></div>
                  </div>
                </div>
              </div>
            </div>
          
        </div>  

   



  <!--        <div class="modal fade" id="sellbook" tabindex="-1" role="dialog" aria-labelledby="sellbookLabel" aria-hidden="true">
                <div class="modal-dialog">
               
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title text-center" id="signUpLabel">Add Details</h4>
                    </div>
                    <div class="modal-body" style="width:70%;margin-left:15%;">
                      <form class="form-horizontal" name="book" novalidate="" enctype="multipart/form-data" role="form">
                       {{csrf_field()}}
      
                      <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-book"></i></span>
                          <input id="bookName" type="text" class="form-control" placeholder="Enter Your BookTitle*...." ng-model="book.bookName">
                          
                        </div>
                        <br>
                        <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-pencil" aria-hidden="true"></i></span>
                          <input id="bookAuthor" type="text" class="form-control" placeholder="Enter Your BookAuthor*...." ng-model="book.bookAuthor">
                        </div><br>
                        <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-inr" aria-hidden="true"></i></span>
                          <input id="bookPrice" type="text" class="form-control" placeholder="Enter Your BookPrice...." ng-model="book.bookPrice">
                        </div>
                     
                        <div class="fileUpload btn btn-warning">
                          <span><i class="fa fa-plus-square" aria-hidden="true"></i></span>
                           <span style="font-size:12px;"><b>Upload</b></span>
                          <input ng-model="book.image" type="file" class="form-control input-lg upload" accept="image/*" file-input="file1" multiple>
                        </div>
                        <div class="fileUpload btn btn-warning">
                          <span><i class="fa fa-plus-square" aria-hidden="true"></i></span>
                           <span style="font-size:12px;"><b>Upload</b></span>
                          <input ng-model="book.image" type="file" class="form-control input-lg upload" accept="image/*" file-input="file2" multiple>
                        </div>
                        <div class="fileUpload btn btn-warning">
                          <span><i class="fa fa-plus-square" aria-hidden="true"></i></span>
                           <span style="font-size:12px;"><b>Upload</b></span>
                          <input ng-model="book.image" type="file" class="form-control input-lg upload" accept="image/*" file-input="file3" multiple>
                        </div>
                        
                        <br><br>
                      </form>
                    </div>
                    <div class="modal-footer">
                      <button type="submit" class="btn btn-success pull-right" style="margin:10px 0px 0px 20px;" ng-click="save(); toggle1()" >submit</button> 
                    </div>
                  </div>
                </div>
              </div>
 -->

    <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="container marketing" style="margin-top:40px;">

      <!-- Three columns of text below the carousel -->
      <div class="row">
        
        @foreach ($books_detials as $books)
          <div class="col-lg-4 data">
            <div class="col-lg-3"></div>
            <div class="col-lg-9">
              <img class="img-circle" src="images/2.jpg" alt="Generic placeholder image" width="140" height="140">
            </div>

            <div class="details">
             <p class="col-md-12">{{$books->bookName}}</p>
                <h1 class="col-md-12">{{$books->bookAuthor}}</h1>
            </div>
            <button type="button" class="btn btn-secondary" ng-click="">Details &raquo;</button>
            <button type="button" class="btn btn-success pull-right" ng-click="">Buy</button>
          </div><!-- /.col-lg-4 -->
        @endforeach
      </div><!-- /.row -->


    

      <!-- START THE FEATURETTES -->

      <hr class="featurette-divider">

      <div class="row featurette">
        <div class="col-md-7">
          <h2 class="featurette-heading">First featurette heading. <span class="text-muted">It'll blow your mind.</span></h2>
          <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
        </div>
        <div class="col-md-5">
          <img class="featurette-image img-responsive center-block" data-src="holder.js/500x500/auto" alt="Generic placeholder image">
        </div>
      </div>

      <hr class="featurette-divider">

      <div class="row featurette">
        <div class="col-md-7 col-md-push-5">
          <h2 class="featurette-heading">Oh yeah, it's that good. <span class="text-muted">See for yourself.</span></h2>
          <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
        </div>
        <div class="col-md-5 col-md-pull-7">
          <img class="featurette-image img-responsive center-block" data-src="holder.js/500x500/auto" alt="Generic placeholder image">
        </div>
      </div>

      <hr class="featurette-divider">

      <div class="row featurette">
        <div class="col-md-7">
          <h2 class="featurette-heading">And lastly, this one. <span class="text-muted">Checkmate.</span></h2>
          <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
        </div>
        <div class="col-md-5">
          <img class="featurette-image img-responsive center-block" data-src="holder.js/500x500/auto" alt="Generic placeholder image">
        </div>
      </div>

      <hr class="featurette-divider">

      <!-- /END THE FEATURETTES -->


      <!-- FOOTER -->
      <footer>
        <p class="pull-right"><a href="#">Back to top</a></p>
        <p>&copy; 2016 Company, Inc. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a></p>
      </footer>

    </div><!-- /.container -->

  

    <!-- angularjs file-->
    <script type="text/javascript" src="js/angular.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.0-rc.0/angular-animate.min.js"></script>
    <script type="text/javascript" src="js/module.js"></script>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="js/bootstrap.min.js"></script>
    
  </body>
</html>
