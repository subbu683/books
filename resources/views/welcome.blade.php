<!doctype html>
<html lang="{{ app()->getLocale() }}" ng-app="booksWorld">
		<head>
				<meta charset="utf-8">
				<meta http-equiv="X-UA-Compatible" content="IE=edge">
				<meta name="viewport" content="width=device-width, initial-scale=1">

				<title>booksjntup</title>

				<!-- Fonts -->
			 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

				<!-- Styles -->
			 <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
			 <link rel="stylesheet" type="text/css" href="css/style.css">
			 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
			 <script type="text/javascript" src="js/jquery-ui.min.js"></script>
			 
		</head>
		 <body ng-controller="Books_ctrl">
     		
		<div class="navbar-wrapper">
			<div class="container">

				<nav class="navbar navbar-static-top">
					<div class="container">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
								<span class="sr-only" style="background:blue;">Toggle navigation</span>
								<span class="icon-bar" style="background:blue;"></span>
								<span class="icon-bar" style="background:blue;"></span>
								<span class="icon-bar" style="background:blue;"></span>
							</button>
							<a class="navbar-brand" href="#">Project name</a>
						</div>
						<div id="navbar" class="navbar-collapse collapse">
							<ul class="nav navbar-nav">
								<li class="active"><a href="#">Home</a></li>
								<li><a href="#about">About</a></li>
								<li><a href="#contact">Contact</a></li>
								 <li>
									 <!-- <button type="button" class="btn btn-primary navbar-btn" ng-click="toggle('1419A'); register=false; login=false" >SellBook</button> -->
									 <button type="submit" class="btn btn-primary navbar-btn" ng-click="sellbook=! sellbook; register=false; login=false">SellBook</button>
								 </li>

							</ul>

							

							<div class="pull-right">
							
							 @if (Auth::check())
								<a href="/logout" ><button class="btn btn-danger navbar-btn">Logout</button></a>
													
							@else
							<button type="submit" class="btn btn-primary navbar-btn" ng-click="login=! login; register=false; sellbook=false">SignIn</button>
							<button type="submit" class="btn btn-primary navbar-btn" ng-click="register=!register; login=false; sellbook=false">SignUp</button>
							@endif	
							</div>
						</div>
					 
					</div>
						
				</nav>

			</div>
		</div>

		@if (Session::has('not_auth'))                
		 	<div class="modal fade" id="warning"  tabindex="-1" role="dialog" aria-labelledby="sellbookLabel" aria-hidden="true">
				<div class="modal-dialog">
					
					<div class="modal-content">
						<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
						<div class="modal-body" style="text-align: center;">
						<h1>
	                       {{Session::get('not_auth')}}
						</h1>
							</div>
					</div>
				</div>
			</div>
  			<script> 
  				$(window).load(function(){
  			  $('#warning').modal('show');
  					
  				});
  			</script>
  			@endif

  			  @if (Session::has('book_success'))                
      <div class="modal fade" id="warning"  tabindex="-1" role="dialog" aria-labelledby="sellbookLabel" aria-hidden="true">
                <div class="modal-dialog">
                
                  <div class="modal-content">
                    <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                    <div class="modal-body" style="text-align: center;">
                    <h1>
                    {{Session::get('book_success')}}
                    </h1>
                      </div>
                  </div>
                </div>
              </div>
        
      
        <script>
          $(window).load(function(){
          $('#warning').modal('show');
            
          });
        </script>
        @endif
  	

			<div class="slide-toggle" ng-show="login">

			<div class="col-md-4 col-sm-4 col-xs-2"></div>
			<div class="col-md-4 col-sm-4 col-xs-8">
				<form method="POST" action="/login" class="form-horizontal" novalidate="">
					 
				 {{csrf_field()}}
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
						<input id="email" type="email" class="form-control" name="email"    placeholder="Enter Your email...." required>
					</div><br>
					<!-- <input type="text" name="name" required> -->
				 
					<div class="input-group">

						<span class="input-group-addon"><i class="fa fa-unlock"></i></span>
						<input id="password" type="password" class="form-control" name="password" placeholder="Enter password...." required="">
					 
					</div><br>
						 <div class="input-group">

					<input type="submit" class="btn btn-success" value="Signin" name="submit" style="margin:0px -40px 0px 20px;">
					 
					</div><br>
					
				 
					<br><br>
				</form>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-2"></div>
			</div> <!--signin-->



			<div class="slide-toggle" ng-show="register">
			 <div class="col-md-4 col-sm-4 col-xs-2"></div>
			<div class="col-md-4 col-sm-4 col-xs-8">
				<form  method="POST" class="form-horizontal" action="/home" novalidate="">
				{{csrf_field()}}
					 <div class="input-group">
						<span class="input-group-addon"><i class="fa fa-id-badge"></i></span>
						<input id="name" type="text" class="form-control" name="name" placeholder="Enter Your name*....">
						
					</div>
					<br>
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-id-badge"></i></span>
						<input id="rollnumber" type="text" class="form-control" name="rollnumber" placeholder="Enter Your Rollnumber*....">
					</div><br>
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
						<input id="email" type="email" class="form-control" name="email"    placeholder="Enter Your email*....">
					 
					</div><br>
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-phone-square"></i></span>
						<input id="phonenumber" type="text" class="form-control" name="phonenumber" placeholder="Enter Your phonenumber....">
					</div><br>
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-unlock"></i></span>
						<input id="password" type="password" class="form-control" name="password" placeholder="Enter password*....">
						
					</div><br>
					<input type="submit" value="SignUp" class="btn btn-success" style="margin:0px -40px 0px 20px;">
					
					<br><br>
				</form>
				</div>
				 <div class="col-md-4 col-sm-4 col-xs-2"></div>
			</div><!--signup-->



			<div class="slide-toggle" ng-show="sellbook" >
			 <div class="col-md-4 col-sm-4 col-xs-2"></div>
			<div class="col-md-4 col-sm-4 col-xs-8">
				<form  method="POST" enctype="multipart/form-data" class="form-horizontal" action="/books" novalidate="">
				{{csrf_field()}}
					 <div style="color: red;font-size: 10px;">
					 	<p>Note:<span>*</span>Please login before advertising!!</p>
					 </div>
					 <div class="input-group">
								<span class="input-group-addon"><i class="fa fa-book"></i></span>
							<input id="bookName" type="text" class="form-control" placeholder="Enter Your name" name="bookName">
								
					</div>

					<br>
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-pencil" aria-hidden="true"></i></span>
								<input id="bookAuthor" type="text" class="form-control" placeholder="Enter Your BookAuthor*...." name="bookAuthor">
							</div><br>
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-inr" aria-hidden="true"></i></span>
								<input id="bookPrice" type="text" class="form-control" placeholder="Enter Your BookPrice...." name="bookPrice">
							</div>
					 
							<div class="fileUpload btn btn-warning">
								<span><i class="fa fa-plus-square" aria-hidden="true"></i></span>
								 <span style="font-size:12px;"><b>Upload</b></span>
								<input  type="file" class="form-control input-lg upload" name="file1" multiple>
							</div>
							<div class="fileUpload btn btn-warning">
								<span><i class="fa fa-plus-square" aria-hidden="true"></i></span>
								 <span style="font-size:12px;"><b>Upload</b></span>
								<input  type="file" class="form-control input-lg upload" name="file2" multiple>
							</div>
							<div class="fileUpload btn btn-warning">
								<span><i class="fa fa-plus-square" aria-hidden="true"></i></span>
								 <span style="font-size:12px;"><b>Upload</b></span>
								<input  type="file" class="form-control input-lg upload" name="file3" multiple>
							</div>

					 <br>
					<input type="submit" value="Submit" class="btn btn-success" style="margin:0px -40px 0px 20px;">
					
					<br><br>
				</form>
				</div>
				 <div class="col-md-4 col-sm-4 col-xs-2"></div>
			</div><!--sellbook-->


			<div class="modal fade" id="buybook" tabindex="-1" role="dialog" aria-labelledby="sellbookLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							</div>
							<div class="modal-body">
								<h2>hdsgkjahjk</h2>
							</div>
							
						</div>
							
						</div>
					</div>

				
			</div>


								<!-- 
					   <div class="modal fade" id="sellbook" tabindex="-1" role="dialog" aria-labelledby="sellbookLabel" aria-hidden="true">
								<div class="modal-dialog">
							
							    <div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<h4 class="modal-title text-center" id="signUpLabel">Add Details</h4>
										</div>
										<div class="modal-body" style="width:70%;margin-left:15%;">
											<form class="form-horizontal" name="book" novalidate="" enctype="multipart/form-data" role="form">
											 {{csrf_field()}}
			
											<div class="input-group">
													<span class="input-group-addon"><i class="fa fa-book"></i></span>
													<input id="bookName" type="text" class="form-control" placeholder="Enter Your BookTitle*...." ng-model="book.bookName">
													
												</div>
												<br>
												<div class="input-group">
													<span class="input-group-addon"><i class="fa fa-pencil" aria-hidden="true"></i></span>
													<input id="bookAuthor" type="text" class="form-control" placeholder="Enter Your BookAuthor*...." ng-model="book.bookAuthor">
												</div><br>
												<div class="input-group">
													<span class="input-group-addon"><i class="fa fa-inr" aria-hidden="true"></i></span>
													<input id="bookPrice" type="text" class="form-control" placeholder="Enter Your BookPrice...." ng-model="book.bookPrice">
												</div>
										 
												<div class="fileUpload btn btn-warning">
													<span><i class="fa fa-plus-square" aria-hidden="true"></i></span>
													 <span style="font-size:12px;"><b>Upload</b></span>
													<input ng-model="book.image" type="file" class="form-control input-lg upload" accept="image/*" file-input="file1" multiple>
												</div>
												<div class="fileUpload btn btn-warning">
													<span><i class="fa fa-plus-square" aria-hidden="true"></i></span>
													 <span style="font-size:12px;"><b>Upload</b></span>
													<input ng-model="book.image" type="file" class="form-control input-lg upload" accept="image/*" file-input="file2" multiple>
												</div>
												<div class="fileUpload btn btn-warning">
													<span><i class="fa fa-plus-square" aria-hidden="true"></i></span>
													 <span style="font-size:12px;"><b>Upload</b></span>
													<input ng-model="book.image" type="file" class="form-control input-lg upload" accept="image/*" file-input="file" multiple>
												</div>
												
												<br><br>
											</form>
										</div>
										<div class="modal-footer">
											<button type="submit" class="btn btn-success pull-right" style="margin:10px 0px 0px 20px;" ng-click="save(); toggle1()" >submit</button> 
										</div>
									</div>
								</div>
							</div>
						 -->

								<!-- <div class="modal fade" id="sellbook" tabindex="-1" role="dialog" aria-labelledby="sellbookLabel" aria-hidden="true">
								<div class="modal-dialog">
								<
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<h4 class="modal-title text-center" id="signUpLabel" style="color: red;font-weight: bold;font-size: 24px;">Sorry</h4>
										</div>
										<div class="modal-body" style="width:70%;margin-left:22%;color: lightblue;font-weight: bold;font-size: 32px;">Please LogIn or SignUp
											</div>
										<div class="modal-footer">
											<button type="submit" class="btn btn-success pull-right" style="margin:10px 0px 0px 20px;" ng-click="toggle1()" >OK</button> 
										</div>
									</div>
								</div>
							</div> -->
						 
				<div class="container-fluid" id="showcase">
				
					<div class="showcase-item showcase-right" id="showcase-1" 
						style="background-image:url('images/1.jpg');">
						<div class="showcase-filter-area"></div>
						<div class="container">
							<div class="row">
								<div class="column col-md-3 col-sm-2 hidden-xs"></div>
								<div class="column col-md-9 col-sm-10 col-xs-12">                      
									<div class="showcase-title"><b></b><p style="color:#e74c3c;font-size:38px;margin-left:250px;"></p></div>
								</div>
							</div>
						</div>
					</div>
				</div>  

				<!--search bar-->
							<form class="form-inline well well-sm clearfix" style="background:#bdc3c7;margin-top:30px;">
									<i class="fa fa-search fa-2x" aria-hidden="true" ></i>
									<input type="text" placeholder="Search for required book..." class="form-control" size="170" ng-model="search" style="padding:20px;margin-left:10px;">
									
							</form>
				<!-- //search bar-->

			 
		<!-- Marketing messaging and featurettes
		================================================== -->
		<!-- Wrap the rest of the page in another container to center all the content. -->

		<div class="container marketing">

			<!-- Three columns of text below the carousel -->
			<div class="row">

				@foreach ($books as $book)
				<a href="/bookdetails ">
					<div class="col-lg-4 data">
						<div class="col-lg-3"></div>
						<div class="col-lg-9">
							<img class="img-circle" src="images/2.jpg" alt="Generic placeholder image" width="140" height="140">
						</div>

						<div class="details">
						 <p class="col-md-12">{{$book->bookName}}</p>
								<h1 class="col-md-12">{{$book->bookAuthor}}</h1>
						</div>
						<button type="button" class="btn btn-secondary" ng-click="">Details &raquo;</button>
			 			<button type="button" class="btn btn-success pull-right">Buy</button>
					</div><!-- /.col-lg-4 -->
					</a>
				@endforeach

			</div><!-- /.row -->

			<div id="about">
				
			</div>

			<!-- FOOTER -->
			<footer>
				<p class="pull-right"><a href="#">Back to top</a></p>
				<p>&copy; 2016 Company, Inc. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a></p>
			</footer>

		</div><!-- /.container -->


		<!-- angularjs file-->
		<script type="text/javascript" src="js/angular.min.js"></script>
		<!-- <script type="text/javascript" src="js/controllers/books_ctrl.js"></script> -->
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.0-rc.0/angular-animate.min.js"></script>
		<script type="text/javascript" src="js/module.js"></script>

		<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		
		<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
		<script src="js/bootstrap.min.js"></script>
		
	</body>
</html>
